var express = require('express');
var api_helper = require('../API_helper')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  api_helper.make_API_call('http://quotes.rest/quote/random.json')
    .then(response => {
        //res.json(response)
        res.render('index', { quote: response.contents.quote, author: response.contents.author });
    })
    .catch(error => {
        res.send(error)
    })
});

module.exports = router;
